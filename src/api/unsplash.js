import axios from 'axios';

export default axios.create({
    baseURL: "https://api.unsplash.com",
    headers: {
        Authorization: 'Client-ID 4938161e97ef26d0703c67348b7b4ea05cae6d047061319ecd43d17a732a41b4'
    }
});